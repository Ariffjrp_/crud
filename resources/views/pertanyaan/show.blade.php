@extends('adminlte.master')

@section('content')
<div class="container-fluid">
    <h2>Show Pertanyaan {{$pertanyaan->id}}</h2>
    <h4>{{$pertanyaan->judul}}</h4>
    <p>{{$pertanyaan->isi}}</p>
    <br>
    <a class="btn btn-info" href="/pertanyaan">Kembali ke daftar pertanyaan</a>
</div>
@endsection